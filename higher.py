import concurrent.futures
import threading
import time
import argparse
import os
import random

from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException

thread_local = threading.local()
URLs = [
    "http://programmer-menulis.blogspot.com",
    "http://mazsupertop.blogspot.com/",
    "http://deloplen.com/afu.php?zoneid=2793891",
    "http://deloplen.com/afu.php?zoneid=2793895",
    "http://deloplen.com/afu.php?zoneid=2793896",
]


def convert_proxies():
    file = open(proxy_file, "r")
    proxies = []

    for line in file:
        line = line.strip().split(":")
        ip_addr = line[0]
        port = int(line[1])
        proxy = [ip_addr, port]
        proxies.append(proxy)

    return proxies


def set_preferences(ip_addr, port):

    profile = webdriver.FirefoxProfile()
    profile.set_preference("network.proxy.type", 1)
    profile.set_preference("network.proxy.socks", ip_addr)
    profile.set_preference("network.proxy.socks_port", port)
    profile.set_preference("network.proxy.socks_version", socks_version)

    return profile


def get_browser(profile):
    if not hasattr(thread_local, "browser"):
        thread_local.browser = webdriver.Firefox(profile)
    return thread_local.browser


def visit_url(proxy):
    ip_addr = proxy[0]
    port = proxy[1]

    extensions = [
        "im_not_robot_captcha_clicker-1.3.xpi",
        "buster_captcha_solver_for_humans-0.6.0.xpi",
        "random_user_agent-2.2.11.xpi"
    ]

    try:
        profile = set_preferences(ip_addr, port)
        browser = get_browser(profile)

        dirpath = os.path.dirname(os.path.realpath(__file__))
        for extension in extensions:
            browser.install_addon(dirpath + "/" + extension, temporary=True)

        print("browsing with ip: %s" % ip_addr)
        for URL in URLs:
            browser.get(URL)
            time.sleep(random.randint(65, 90))
        browser.quit()
    except TimeoutException:
        print("%s Raised TimeoutException" % ip_addr)
        time.sleep(15)
        browser.quit()
    except WebDriverException:
        print("%s Raised WebDriverException" % ip_addr)
        time.sleep(15)
        browser.quit()


def queue_visit():
    demand = 0
    proxy_wait = []

    i = 0
    while i < proxy_count + 1:
        if (demand < 7 and i < proxy_count):
            proxy_wait.append(proxy_lists[i])
            demand += 1
            print("Preparing connection %i from %i with ip: %s" %
                  (i + 1, proxy_count, proxy_lists[i][0]))
        else:
            with concurrent.futures.ThreadPoolExecutor(max_workers=7) as executor:
                executor.map(visit_url, proxy_wait)
            proxy_wait = []
            demand = 0
            i -= 1
        i += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "proxy_file",
        help="File containt proxy lists",
        type=str
    )
    parser.add_argument(
        "socks_version",
        help="Socks version in used file",
        type=int
    )
    args = parser.parse_args()

    proxy_file = args.proxy_file
    socks_version = args.socks_version
    print("Using proxies v%i from %s" % (socks_version, proxy_file))
    proxy_lists = convert_proxies()
    proxy_count = len(proxy_lists)
    print("Found %i proxies" % proxy_count)
    queue_visit()
