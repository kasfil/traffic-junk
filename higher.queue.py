from threading import Thread
import time

from queue import Queue
from selenium import webdriver

url = "http://mazsupertop.blogspot.com"
q = Queue(maxsize=0)
num_threads = 5


def prepare_proxy():
    # get proxy from file

def set_preferences(ip_addr, port):
    profile = webdriver.FirefoxProfile()
    profile.set_preference("network.proxy.type", 1)
    profile.set_preference("network.proxy.socks", ip_addr)
    profile.set_preference("network.proxy.socks_port", port)
    profile.set_preference("network.proxy.socks_version", 5)
    return profile


def get_browser(profile):
    browser = webdriver.Firefox(profile)
    browser.get(url)
    time.sleep(30)
    browser.quit()


def visit_url(q, proxy):
    print(q.get(timeout=3000))
    ip_addr = proxy[0]
    port = proxy[1]
    profile = set_preferences(ip_addr, port)
    print("browsing with %s" % ip_addr)
    get_browser(profile)
    q.task_done()


def queue_visit():
    for i in range(len(proxy_lists)):
        worker = Thread(target=visit_url, args=(q, i))
        worker.setDaemon(True)
        worker.start()


if __name__ == "__main__":
    queue_visit()
